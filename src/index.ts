import path, { join } from 'path';
import * as fs from 'fs';
import { IBdExt, Level, Level1, Level2, Level3 } from './bd-ext.interface';
import * as XLSX from 'xlsx';

interface IMissingLevels {
  missingLevels1: string[];
  missingLevels2: Map<string, string[]>;
  missingLevels3: Map<string, Map<string, string[]>>;
  missingLevels4: Map<string, Map<string, Map<string, string[]>>>;
}

interface IBdExtMissingLevels {
  collections: IMissingLevels;
  locations: IMissingLevels;
  dates: IMissingLevels;
}

function checkLevel1(
  name: 'name' | 'title',
  sourceLevels1: Level1[],
  destLevels1: Level1[],
): IMissingLevels {
  const missingLevels1: string[] = [];
  const missingLevels2 = new Map<string, string[]>();
  const missingLevels3 = new Map<string, Map<string, string[]>>();
  const missingLevels4 = new Map<string, Map<string, Map<string, string[]>>>();
  for (const sourceLevel1 of sourceLevels1) {
    const destLevel1 = destLevels1.find(
      (lvl1) => lvl1[name].toLowerCase() === sourceLevel1.name.toLowerCase(),
    );
    if (!destLevel1) {
      missingLevels1.push(sourceLevel1[name].toLowerCase());
    }
    const resultCheckLevel2 = checkLevel2(
      name,
      sourceLevel1,
      destLevel1?.level2,
    );
    if (resultCheckLevel2.missingLevels2.length > 0) {
      missingLevels2.set(
        sourceLevel1[name].toLowerCase(),
        resultCheckLevel2.missingLevels2,
      );
    }
    if (resultCheckLevel2.missingLevels3.size > 0) {
      missingLevels3.set(
        sourceLevel1[name].toLowerCase(),
        resultCheckLevel2.missingLevels3,
      );
    }
    if (resultCheckLevel2.missingLevels4.size > 0) {
      missingLevels4.set(
        sourceLevel1[name].toLowerCase(),
        resultCheckLevel2.missingLevels4,
      );
    }
  }
  return { missingLevels1, missingLevels2, missingLevels3, missingLevels4 };
}

function checkLevel2(
  name: 'name' | 'title',
  sourceLevel1: Level1,
  destLevels2: Level2[] | undefined,
) {
  const missingLevels2: string[] = [];
  const missingLevels3 = new Map<string, string[]>();
  const missingLevels4 = new Map<string, Map<string, string[]>>();
  if (sourceLevel1.level2) {
    for (const sourceLevel2 of sourceLevel1.level2) {
      const destLevel2 = destLevels2?.find(
        (lvl2) => lvl2[name].toLowerCase() === sourceLevel2.name.toLowerCase(),
      );
      if (!destLevel2) {
        missingLevels2.push(sourceLevel2[name].toLowerCase());
      }
      const resultCheckLevel3 = checkLevel3(
        name,
        sourceLevel2.level3,
        destLevel2?.level3,
      );
      if (resultCheckLevel3.missingLevels4.size > 0) {
        missingLevels4.set(
          sourceLevel2[name].toLowerCase(),
          resultCheckLevel3.missingLevels4,
        );
      }
      if (resultCheckLevel3.missingLevels3.length > 0) {
        missingLevels3.set(
          sourceLevel2[name].toLowerCase(),
          resultCheckLevel3.missingLevels3,
        );
      }
    }
  }
  return {
    missingLevels2,
    missingLevels3,
    missingLevels4,
  };
}

function checkLevel3(
  name: 'name' | 'title',
  sourceLevels3: Level3[] | undefined,
  destLevels3: Level3[] | undefined,
) {
  const missingLevels3: string[] = [];
  const missingLevels4 = new Map<string, string[]>();
  if (sourceLevels3) {
    for (const sourceLevel3 of sourceLevels3) {
      const destLevel3 = destLevels3?.find(
        (lvl3) => lvl3[name].toLowerCase() === sourceLevel3.name.toLowerCase(),
      );
      if (!destLevel3) {
        missingLevels3.push(sourceLevel3[name].toLowerCase());
      }
      const resultCheckLevel4 = checkLevel4(
        name,
        sourceLevel3.level4,
        destLevel3?.level4,
      );
      if (resultCheckLevel4.length > 0) {
        missingLevels4.set(sourceLevel3[name].toLowerCase(), resultCheckLevel4);
      }
    }
  }
  return {
    missingLevels3,
    missingLevels4,
  };
}

function checkLevel4(
  name: 'name' | 'title',
  sourceLevels4: Level[] | undefined,
  destLevels4: Level[] | undefined,
) {
  const missingLevels4: string[] = [];
  if (sourceLevels4) {
    for (const sourceLevel4 of sourceLevels4) {
      if (destLevels4) {
        const destLevel4 = destLevels4.find(
          (lvl4) =>
            lvl4[name].toLowerCase() === sourceLevel4.name.toLowerCase(),
        );
        if (!destLevel4) {
          missingLevels4.push(sourceLevel4[name].toLowerCase());
        }
      } else {
        missingLevels4.push(sourceLevel4[name].toLowerCase());
      }
    }
  }
  return missingLevels4;
}

function checkBdExt(source: IBdExt, dest: IBdExt): IBdExtMissingLevels {
  const name = dest.iso === 'eng' ? 'title' : 'name';
  const collections = checkLevel1(name, source.collections, dest.collections);
  const locations = checkLevel1(name, source.locations, dest.locations);
  const dates = checkLevel1(name, source.dates, dest.dates);
  return {
    collections,
    locations,
    dates,
  };
}

function newExcelLines(
  source: IBdExt,
  dest: IBdExt,
  collections: IMissingLevels,
  locations: IMissingLevels,
  dates: IMissingLevels,
) {
  const iso = dest.iso;
  const workbook = XLSX.utils.book_new();
  const worksheet = workbook.Sheets['Sheet1'];
  const sheet1 = XLSX.utils.sheet_add_aoa(
    worksheet,
    [
      ['COLLECTIONS'],
      [
        'Niveau1',
        'Niveau2',
        'Niveau3',
        'Niveau4',
        'DATE',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
      ],
      ...getLines(source, dest, iso, collections),
      [],
      ['LOCATIONS'],
      [
        'Niveau1',
        'Niveau2',
        'Niveau3',
        'Niveau4',
        'DATE',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
      ],
      ...getLines(source, dest, iso, locations),
      [],
      ['DATES'],
      [
        'Niveau1',
        'Niveau2',
        'Niveau3',
        'Niveau4',
        'DATE',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
      ],
      ...getLines(source, dest, iso, dates),
    ],
    {
      origin: 'A1',
    },
  );
  XLSX.utils.book_append_sheet(workbook, sheet1);
  XLSX.writeFile(workbook, `outputs/${iso}.xlsx`);
}

function getLines(
  sourceContent: IBdExt,
  destContent: IBdExt,
  iso: string,
  maps: IMissingLevels,
) {
  const title = iso === 'eng' ? 'name' : 'title';
  const name = iso === 'eng' ? 'title' : 'name';
  const source = iso === 'eng' ? sourceContent : destContent;
  const lines: string[][] = [];
  for (const missingLevel1 of maps.missingLevels1) {
    lines.push(['?', '', '', '', '', missingLevel1]);
  }

  for (const [level1, missingLevels2] of maps.missingLevels2) {
    for (const missingLevel2 of missingLevels2) {
      const niveau1 = source.collections.find(
        (lvl1) => lvl1[name].toLowerCase() === level1,
      );
      const niv1 = niveau1 ? niveau1[title] : '?';
      lines.push([niv1, '?', '', '', '', level1, missingLevel2]);
    }
  }

  for (const [level1, missingLevels3Lvl1] of maps.missingLevels3) {
    const niveau1 = source.collections.find(
      (lvl1) => lvl1[name].toLowerCase() === level1,
    );
    const niv1 = niveau1 ? niveau1[title] : '?';
    for (const [level2, missingLevels3] of missingLevels3Lvl1) {
      const niveau2 = niveau1?.level2?.find(
        (lvl2) => lvl2[name].toLowerCase() === level2,
      );
      const niv2 = niveau2 ? niveau2[title] : '?';
      for (const missingLevel3 of missingLevels3) {
        lines.push([niv1, niv2, '?', '', '', level1, level2, missingLevel3]);
      }
    }
  }

  for (const [level1, missingLevels4Lvl1] of maps.missingLevels4) {
    const niveau1 = source.collections.find(
      (lvl1) => lvl1[name].toLowerCase() === level1,
    );
    const niv1 = niveau1 ? niveau1[title] : '?';
    for (const [level2, missingLevels4Lvl2] of missingLevels4Lvl1) {
      const niveau2 = niveau1?.level2?.find(
        (lvl2) => lvl2[name].toLowerCase() === level2,
      );
      const niv2 = niveau2 ? niveau2[title] : '?';
      for (const [level3, missingLevels4] of missingLevels4Lvl2) {
        const niveau3 = niveau2?.level3?.find(
          (lvl3) => lvl3[name].toLowerCase() === level3,
        );
        const niv3 = niveau3 ? niveau3[title] : '?';
        for (const missingLevel4 of missingLevels4) {
          lines.push([
            niv1,
            niv2,
            niv3,
            '?',
            '',
            level1,
            level2,
            level3,
            missingLevel4,
          ]);
        }
      }
    }
  }
  return lines;
}

function bootstrap() {
  const bdExts = fs.readdirSync(path.join(__dirname, '../bdExts'));
  for (const bdExt of bdExts) {
    const sourceFilePath = join(__dirname, '../source.fra.json');
    const destFilePath = join(__dirname, `../bdExts/${bdExt}`);

    const sourceContent = JSON.parse(
      fs.readFileSync(sourceFilePath).toString(),
    ) as IBdExt;
    const destContent = JSON.parse(
      fs.readFileSync(destFilePath).toString(),
    ) as IBdExt;

    const { collections, locations, dates } = checkBdExt(
      sourceContent,
      destContent,
    );
    newExcelLines(sourceContent, destContent, collections, locations, dates);
  }

  console.log('End');
}

bootstrap();
