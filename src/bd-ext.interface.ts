export interface IBdExt {
  iso: string;
  collections: Level1[];
  locations: Level1[];
  dates: Level1[];
}

export interface Level {
  name: string;
  title: string;
}

export interface Level1 extends Level {
  level2?: Level2[];
}

export interface Level2 extends Level {
  level3?: Level3[];
}

export interface Level3 extends Level {
  level4?: Level[];
}
