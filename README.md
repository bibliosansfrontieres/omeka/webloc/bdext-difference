# How to use

- Create a folder `bdExts`
- Create a folder `outputs`
- Create a file `source.fra.json`

In `bdExts` folder, you need to put all the JSON BDExt files.

In the file `source.fra.json`, you need to put the content of the french JSON BDExt file.

Just run the script with `npm run start:dev`.

This will generate translation files in the folder ̀`outputs`.
